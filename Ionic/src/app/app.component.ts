import { MenuPage } from './../pages/menu/menu';
import { Component, ViewChild  } from '@angular/core';
import { Platform,NavController,Nav, Events} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AccountService} from '../services/account-service';
import { Keyboard } from '@ionic-native/keyboard';
import { Login } from '../pages/login/login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;
  @ViewChild(Nav) navCtrl: Nav;
  constructor(public keyboard:Keyboard,public events: Events,public platform: Platform, public statusBar: StatusBar,public splashScreen: SplashScreen, public accountService: AccountService) {
    platform.ready().then(() => {
      
      this.accountService.getUserId(data=> {
      if(data==0){this.rootPage = Login;}
      else{this.rootPage=MenuPage;}
      })
     })
 
  }
 
}
