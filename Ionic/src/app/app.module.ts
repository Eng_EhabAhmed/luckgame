import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage'
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import {RoundProgressModule} from 'angular-svg-round-progressbar';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//services

import { AccountService } from '../services/account-service';
import { GameService } from '../services/game-service'

import {HttpService} from '../services/http-service';


import { Login } from '../pages/login/login';

import { Register } from '../pages/register/register';
import {MenuPage} from '../pages/menu/menu';
import {GamePage} from '../pages/game/game';
import { ListPage } from '../pages/list/list';



import { Diagnostic } from '@ionic-native/diagnostic';
import { Device } from '@ionic-native/device';
import { Keyboard } from '@ionic-native/keyboard';



@NgModule({
  declarations: [
    MyApp,
    Login,
    MenuPage,
    GamePage,
    Register,
    ListPage
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RoundProgressModule,
    IonicStorageModule.forRoot(), 
    IonicModule.forRoot(MyApp,{mode:'md'})
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Login,
    Register,
    MenuPage,
    GamePage,
    ListPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AccountService,
    GameService,
    InAppBrowser,
    HttpService,
    Diagnostic,
    Device,
    Keyboard,
    {provide: ErrorHandler, useClass: IonicErrorHandler,}
  ]
})
export class AppModule {}
