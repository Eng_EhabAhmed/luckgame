import {Injectable} from "@angular/core";
import {HttpService} from '../services/http-service';
import { Storage } from '@ionic/storage';


@Injectable()
export class AccountService {
    public city: any;
   
/**
*ّInitialize account Service Data
*/
    public user: {
        email: string,
        password: string,       
        confirm:boolean
    } = { email: "", password: "", confirm:false};
    constructor(  public httpService: HttpService, public storage: Storage ){          
    }
    showAlert(message) {
        this.httpService.showAlert(message);
    }
   

        clearstorage()
        {
            this.storage.set('userId',null);
            this.storage.set('token',null);
        }
        /**
        * get User Id 
        */
        getUserId(callback) {           
            this.storage.get('userId').then((val) => {
                if (!((val == "null") || (val == null))) {
                    callback(val);
                }
                else
                    callback(0);
            })
        }
   
    getUsertoken(callback) {           
        this.storage.get('token').then((val) => {
           
            if (!((val == "null") || (val == null))) {
                callback(val);
            }
            else
                callback(0);
        })
    }
        /**
        * logout and clear storage
        */
    logout() {
        this.user = { email: "", password: "", confirm:false};
        this.storage.set('userId', this.user);
        this.storage.set('token', "");  
        }
    
/**
*  register new user 
* 
* @param {email ,password} savedUser
* @returns {User Object}
*/
    register(data, callback) {
      
            this.httpService.showLoading();
            let main = this;        
            this.httpService.post('register', data, result => {
                this.httpService.hideLoading();
                if(result==false)
                {
                    this.showAlert("this email is alreay exist");
                }
                 else {
                console.log(result);
               
                this.storage.set('userId', result.id);
                this.storage.set('token', result.token);  
                callback();
            }
            });    
        
    }
/**
*  login  user 
* 
* @param {email ,password} savedUser
* @returns {User Object}
*/

    login(data, callback) {
    
            this.httpService.showLoading();
            let main = this;       
            this.httpService.put('login', data, result => {
                this.httpService.hideLoading();
                if(result==false)
                    {
                        this.showAlert("error in email or password please try again");

                        //alert('error in email or password please try again');
                    }
                else {
                    console.log(result);
                    main.user = data; 
                    main.storage.set('userId', result.id);
                    this.storage.set('token', result.token);  
                    callback();
                }
            });
              
    }


}