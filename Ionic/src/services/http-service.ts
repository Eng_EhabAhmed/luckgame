import {Injectable} from "@angular/core";
import { LoadingController, AlertController } from 'ionic-angular';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpService {
    private serviceUrl: string= 'http://165.227.220.162:8000/api/';
    
    private config:any;
    public loader:any;
    
    constructor(public alertCtrl: AlertController,public http: Http, public loadingCtrl: LoadingController) {
        this.config = { headers : {'Content-Type': 'application/json;charset=utf-8'} };       
    }
 
    get(url,callback) { 
        this.http.get(this.serviceUrl+url).map(res => res.json()).subscribe(data => {           
            callback(data);           
        },(err) => {
             this.showError(err);
         });
    }    
    post(url, body, callback){       
         this.http.post(this.serviceUrl+url,JSON.stringify(body),this.config).map(res => res.json()).subscribe(data => {
            callback(data);           
        },(err) => {
             this.showError(err);
         });
    } 

    put(url, body, callback){
         this.http.put(this.serviceUrl+url,JSON.stringify(body),this.config).map(res => res.json()).subscribe(data => {
            callback(data);           
        },(err) => {

             this.showError(err);
         });
    } 
    showAlert(message) {
        var alert = this.alertCtrl.create({           
            message: message,
            cssClass: 'showAlert',
            buttons: ['OK']
        });
        alert.present();
    }
    showLoading() {
        this.loader = this.loadingCtrl.create({
            content: "Loading",
            spinner: "ios"
        });
        this.loader.present();
    }
    
    hideLoading() {
        this.loader.dismiss();
    }
     showError(err){
        this.hideLoading();
        this.showAlert(err);
    }
}