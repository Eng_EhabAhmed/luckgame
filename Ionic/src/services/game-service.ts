import {Injectable} from "@angular/core";
import {HttpService} from '../services/http-service';
@Injectable()
export class GameService {
    constructor(  public httpService: HttpService){          
    }
    showAlert(message) {
        this.httpService.showAlert(message);
    }
    /**
    *  Start new game 
    * 
    * @param {User token ,user id} savedUser
    * @returns {Game id}
    */
    startgame(data, callback) {
        this.httpService.showLoading();
         let main = this;        
         this.httpService.post('games', data, result => {
             this.httpService.hideLoading();
             if(result==false){}
             else { callback(result);}
         });}
          /**
            *  End  game 
            * 
            * @param {User token ,game id,array of attempts} savedUser
            * @returns {Success true }
            */
      endgame(data, callback) {
             this.httpService.showLoading();
              let main = this;        
              this.httpService.put('games/'+data.game_id, data, result => {
                  this.httpService.hideLoading();
                  if(result==false){}
                  else { 
                    this.showAlert("Game Ended");  
                    callback(result);}
              });}
 /**
    *  Get Statistics for games 
    * 
    * @param {User token ,user id} savedUser
    * @returns { Statistic Object}
    */
     statistic(data, callback) {
                 this.httpService.showLoading();
                  let main = this;        
                  this.httpService.get('games/'+data, result => {
                      this.httpService.hideLoading();
                      if(result==false){}
                      else { callback(result);}
                  });}
}