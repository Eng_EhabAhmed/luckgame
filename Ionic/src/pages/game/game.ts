import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {RoundProgressModule, RoundProgressConfig} from 'angular-svg-round-progressbar';
import {ListPage} from '../list/list';
import { AccountService} from '../../services/account-service';
import { GameService } from '../../services/game-service';

@Component({
  selector: 'page-game',
  templateUrl: 'game.html'
})
export class GamePage {

/**
* Initialize Game Object Data
*/
public currentvalue:number;
public maxvalue:number;
public specificValue:number;
public GameId:number;
public interval:any;
public IntervalSecondsCount:any;
public attempt_number:number;
public attempt_Count:number;
public UserId:any;
public Token:any;
public LabelText:string;
public attempt_array : Array<any>;
public data: {
  startGameVisible: boolean,
} = { startGameVisible: true};
  constructor(public navCtrl: NavController,private _config: RoundProgressConfig,public accountService: AccountService,public gameservice: GameService) {
    this.accountService.getUsertoken(token=>
      {
        // console.log('this.Token'+token);
        this.Token=token;
         this.accountService.getUserId(Id=>
        {
          this.UserId=Id;
        });
      });
  
  }

/**
* calcualte Interval Seconds according to attempt number
*/
calculateIntervalSecondsCounter()
{
  if(this.attempt_number==1)
  {
   this.IntervalSecondsCount=1000/3;
  
  }
  else if(this.attempt_number==2)
  {
    this.IntervalSecondsCount=1000/4;
  }
  else if(this.attempt_number==3)
  {
    this.IntervalSecondsCount=1000/5;

  }
}

/**
*Interval run 
*/
StartCounter()
{

/**
* get seconds for running Interval
*/
  this.calculateIntervalSecondsCounter();

/**
start Interval 
*/
  this.interval=   setInterval(() => {    
    if(this.currentvalue>this.specificValue)
    {
      clearInterval(this.interval);
      this.attempt_array.push(
        {
          "attempt_no":this.attempt_Count,
          "attempt_time": Math.floor(this.currentvalue*this.IntervalSecondsCount)
        }
      )
      this.attempt_Count +=1;
      this.attempt_number=1;
      this.currentvalue=0;
      this.StartCounter();
    }
    else
    {
      this.currentvalue +=1;
    }
   
},this.IntervalSecondsCount);
}

/**
* stop Counter function when click in Game 
*/
  StopCounter()
  {
    clearInterval(this.interval);

    this.attempt_array.push(
      {
        "attempt_no":this.attempt_Count,
        "attempt_time": Math.floor(this.currentvalue*this.IntervalSecondsCount)
      }
    )
    this.attempt_Count+=1;
    if(this.currentvalue==this.specificValue)
    {
        if(this.attempt_number == 3)
        {
          // End Game
          this.EndGame();
        }
        else
        {
      
          this.attempt_number += 1;

          this.StartCounter();
        }
    }
    else
    {

      this.attempt_number=1;
      this.StartCounter();
    }
    this.currentvalue=0;

  }

  StartGame()
  {
   
        var data=
        {
          "user_id":this.UserId,
          "token":this.Token
        }
        this.gameservice.startgame(data,result=>
        {
          this.currentvalue=0;
          this.maxvalue=result.a;
          this.specificValue=result.b;
          this.GameId=result.game_id;
          this.attempt_number=1;
          this.attempt_Count=1;
          this.attempt_array =[];
          this.data={ startGameVisible: false};
         this.LabelText="You need to click on number "+ this.specificValue+ " on circle 3 times Sequentially";
          this.StartCounter();
        });

  }
  EndGame()
  {
    var data=
    {
      "game_id":this.GameId,
      "attempts":this.attempt_array,
      "token":this.Token
    }
    this.data={ startGameVisible: true};
    this.gameservice.endgame(data,result=>
      {
      });
  
  }

/**
* Go To statistic Game
*/
  StatisticGame()
  {
    this.navCtrl.push(ListPage);
  }
  
}
