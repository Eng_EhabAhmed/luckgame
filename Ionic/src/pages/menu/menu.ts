import { Component, ViewChild } from '@angular/core';
import { Nav, Platform,NavController,NavParams} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GamePage } from '../game/game';
import { Login } from '../login/login';
import { AccountService} from '../../services/account-service';

@Component({
  templateUrl: 'menu.html'
})
export class MenuPage {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = GamePage;
  pages: Array<{title: string, component: any}>;

  constructor(public navCtrl: NavController ,public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,public navParams: NavParams, public accountService: AccountService) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Start New Game', component: GamePage }
    ];


  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  LogOut()
  {
    this.accountService.clearstorage();
  //  this.storage.set('Name', null);
  //        this.storage.set('Picture', null);
  this.navCtrl.setRoot(Login);
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario

    this.nav.setRoot(page.component);
  }
}
