import { GameService } from '../../services/game-service';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AccountService} from '../../services/account-service';


@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  Token:any;
  UserId:any;
   arrayitems:any;
   showValue:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams,public accountService: AccountService,public gameservice: GameService) {
    this.accountService.getUsertoken(token=>
      {
        this.Token=token;
         this.accountService.getUserId(Id=>
        {
          this.UserId=Id;

          this.gameservice.statistic(this.UserId+'/'+this.Token,result=>
            {
          //    this.arrayitems={"totals":{"total":2,"ended":1},"games_time":{"average":204,"best":"102","worst":"102"},"games_attempts":{"average":6,"best":3,"worst":3},"rank":{"me":2,"total":3}}
          this.arrayitems=result;
          this.showValue=true;
        });
        });
      });
 
  }

  goBack()
  {
    this.navCtrl.pop();
  }
}
