import { Component } from '@angular/core';
import {  NavController , NavParams} from 'ionic-angular';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { AccountService} from '../../services/account-service';
import { Register } from '../register/register';
import {MenuPage} from '../menu/menu';


//@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {

/**
*Initialize Login Form Data
*/
  public IsArabic:boolean = true;
  public data: {
    email: string,    
    password: string,

  } = { email: "",password: ""};
  public MyForm = new FormGroup({
    email: new FormControl(),     
    password: new FormControl()
  });
   constructor( public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, public accountService: AccountService) {

/**
* Make Validation On Form Control
*/
    this.MyForm = this.formBuilder.group({
         email: ['', Validators.compose([Validators.required, this.emailValidator.bind(this)])],
         password: ['', Validators.compose([Validators.minLength(3), Validators.maxLength(15), Validators.pattern('[0-9a-zA-Zا-ي_-]*'), Validators.required])]
     });
    }

  emailValidator(control: FormControl): { [s: string]: boolean } {
        if (!(control.value.toLowerCase().match('^[0-9a-zA-Z._-]*\\@[0-9a-zA-Z_-]*\\.[a-z.]*$'))) {          
           return { invalidEmail: true };
      }
  }

  login() {
    
    // this.navCtrl.push(MenuPage);

    this.accountService.login(this.data, result => {
      this.navCtrl.push(MenuPage);

      });
  }
  goBack(){
    this.navCtrl.pop();
  }

 
  goToRegister() {
    this.navCtrl.push(Register);
    
  }


  
}
