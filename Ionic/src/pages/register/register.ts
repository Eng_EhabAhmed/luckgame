import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { AccountService} from '../../services/account-service';
import {MenuPage} from '../menu/menu';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class Register {


/**
* Initialize Register data
*/
  public data: {
    email: string,    
    password: string,
    confirmpassword:string
  } = { email: "",password: "",confirmpassword:""};
  
  public MyForm = new FormGroup({
    email: new FormControl(),     
    password: new FormControl(),
    confirmpassword: new FormControl(),
  });

  constructor(public navParams: NavParams,public navCtrl: NavController, private formBuilder: FormBuilder, public accountService: AccountService ) {
     
/**
set Validation On Data.
*/
    this.MyForm = this.formBuilder.group({
        email: ['', Validators.compose([Validators.required, this.emailValidator.bind(this)])],
        password: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(15), Validators.pattern('[0-9a-zA-Zا-ي_-]*'), Validators.required])],
        confirmpassword: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(15), Validators.pattern('[0-9a-zA-Zا-ي_-]*'), Validators.required])]
     });
  }

  
  emailValidator(control: FormControl): { [s: string]: boolean } {
    if (!(control.value.toLowerCase().match('^[0-9a-zA-Z._-]*\\@[0-9a-zA-Z_-]*\\.[a-z.]*$'))) {      
         return { invalidEmail: true };
      }
  }

    register() {   

/**
* Will register New User according his email and password.
* 
* @param {} Form Data
* @returns {Promise<User[]>}
*/

      this.accountService.register(this.data, result => {
        this.navCtrl.push(MenuPage);

        });    
    }
  // button for return back 
  goBack(){
      this.navCtrl.pop();
  }

 

}

